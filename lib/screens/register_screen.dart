import 'dart:ui';

import 'package:flutter/material.dart';
import '../screens/login_screen.dart';

class RegisterScreen extends StatelessWidget {
  static const routeName = '/register-screen';
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: _height * 0.45,
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(_height * 0.07))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: _height * 0.05,
                ),
                Image.asset(
                  'assets/images/splashPic.png',
                  fit: BoxFit.contain,
                  width: double.infinity,
                  height: _height * 0.40,
                )
              ],
            ),
          ),
          Container(
            child: Column(
              children: [
                SizedBox(
                  height: _height * 0.01,
                ),
                Text(
                  'ثبت نام',
                  style: TextStyle(
                      fontSize: _height * 0.04, color: Colors.black87),
                ),
                SizedBox(
                  height: _height * 0.02,
                ),
                Padding(
                  padding: EdgeInsets.all(_height * 0.001),
                  child: Container(
                    width: _width * 0.9,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black54,
                            style: BorderStyle.solid,
                            width: _width * 0.004),
                        borderRadius: BorderRadius.circular(29)),
                    child: TextFormField(
                        style: TextStyle(
                            color: Colors.black, fontSize: _height * 0.03),
                        textAlign: TextAlign.right,
                        textDirection: TextDirection.rtl,
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding:
                                EdgeInsets.only(right: _height * 0.03),
                            // border: OutlineInputBorder( underestand code
                            //     borderRadius: BorderRadius.circular(29)),
                            hintText: 'نام کاربری',
                            hintTextDirection: TextDirection.rtl,
                            hintStyle: TextStyle(
                                color: Colors.black45,
                                fontSize: _height * 0.03))),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(_height * 0.02),
                  child: Container(
                    width: _width * 0.9,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black54,
                            style: BorderStyle.solid,
                            width: _width * 0.004),
                        borderRadius: BorderRadius.circular(29)),
                    child: TextFormField(
                        style: TextStyle(
                            color: Colors.black, fontSize: _height * 0.03),
                        textAlign: TextAlign.right,
                        textDirection: TextDirection.rtl,
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding:
                                EdgeInsets.only(right: _height * 0.03),
                            // border: OutlineInputBorder( underestand code
                            //     borderRadius: BorderRadius.circular(29)),
                            hintText: 'پسورد',
                            hintTextDirection: TextDirection.rtl,
                            hintStyle: TextStyle(
                                color: Colors.black45,
                                fontSize: _height * 0.03))),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(_height * 0.002),
                  child: Container(
                    width: _width * 0.9,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black54,
                            style: BorderStyle.solid,
                            width: _width * 0.004),
                        borderRadius: BorderRadius.circular(29)),
                    child: TextFormField(
                        style: TextStyle(
                            color: Colors.black, fontSize: _height * 0.03),
                        textAlign: TextAlign.right,
                        textDirection: TextDirection.rtl,
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding:
                                EdgeInsets.only(right: _height * 0.03),
                            // border: OutlineInputBorder( underestand code
                            //     borderRadius: BorderRadius.circular(29)),
                            hintText: 'ایمیل',
                            hintTextDirection: TextDirection.rtl,
                            hintStyle: TextStyle(
                                color: Colors.black45,
                                fontSize: _height * 0.03))),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(_height * 0.02),
                  child: Container(
                    width: _width * 0.9,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black54,
                            style: BorderStyle.solid,
                            width: _width * 0.004),
                        borderRadius: BorderRadius.circular(29)),
                    child: TextFormField(
                        style: TextStyle(
                            color: Colors.black, fontSize: _height * 0.03),
                        textAlign: TextAlign.right,
                        textDirection: TextDirection.rtl,
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding:
                                EdgeInsets.only(right: _height * 0.03),
                            // border: OutlineInputBorder( underestand code
                            //     borderRadius: BorderRadius.circular(29)),
                            hintText: 'شماره ی موبایل',
                            hintTextDirection: TextDirection.rtl,
                            hintStyle: TextStyle(
                                color: Colors.black45,
                                fontSize: _height * 0.03))),
                  ),
                ),
                ElevatedButton(
                  child: Text(
                    'ورود',
                    style: TextStyle(
                        fontSize: _height * 0.04, color: Colors.black),
                  ),
                  style: ElevatedButton.styleFrom(
                      fixedSize: Size(_width * 0.9, _height * 0.06),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(29),
                      ),
                      primary: Theme.of(context).primaryColor),
                  onPressed: () {
                    //Main Navigation
                  },
                ),
                SizedBox(
                  height: _height * 0.03,
                ),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'حساب کاربری دارید؟',
                        style: TextStyle(
                            fontSize: _height * 0.03, color: Colors.black54),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .pushReplacementNamed(LoginScreen.routeName);
                        },
                        child: Text(
                          'ورود',
                          style: TextStyle(
                              color: Colors.blue[700],
                              fontSize: _height * 0.03,
                              fontFamily: 'BNaznnBd'),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    ));
  }
}
