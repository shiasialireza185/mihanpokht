import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../screens/register_screen.dart';

class LoginScreen extends StatelessWidget {
  static const routeName = '/login-form';
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: _height * 0.48,
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(_height * 0.07))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: _height * 0.05,
                ),
                Image.asset(
                  'assets/images/splashPic.png',
                  fit: BoxFit.contain,
                  width: double.infinity,
                  height: _height * 0.40,
                )
              ],
            ),
          ),
          Container(
            child: Column(children: [
              SizedBox(
                height: _height * 0.02,
              ),
              Text(
                'ورود به نرم افزار',
                textDirection: TextDirection.rtl,
                style:
                    TextStyle(fontSize: _height * 0.04, color: Colors.black87),
              ),
              SizedBox(
                height: _height * 0.03,
              ),
              Padding(
                padding: EdgeInsets.all(_height * 0.001),
                child: Container(
                  width: _width * 0.9,
                  decoration: BoxDecoration(
                      border: Border.all(
                          width: _width * 0.004,
                          style: BorderStyle.solid,
                          color: Colors.black54),
                      borderRadius: BorderRadius.circular(29)),
                  child: TextField(
                    style: TextStyle(
                        color: Colors.black, fontSize: _height * 0.03),
                    textDirection: TextDirection.rtl,
                    textAlign: TextAlign.right,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(right: _height * 0.03),
                      border: InputBorder.none,
                      hintText: 'نام کاربری',
                      hintStyle: TextStyle(
                          color: Colors.black45, fontSize: _height * 0.03),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: _height * 0.002,
              ),
              Padding(
                padding: EdgeInsets.only(top: _height * 0.02),
                child: Container(
                  width: _width * 0.9,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(29),
                      border: Border.all(
                          width: _width * 0.004,
                          color: Colors.black54,
                          style: BorderStyle.solid)),
                  child: TextField(
                    style: TextStyle(
                        color: Colors.black, fontSize: _height * 0.03),
                    textDirection: TextDirection.rtl,
                    textInputAction: TextInputAction.done,
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                        hintText: 'پسورد',
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.only(right: _height * 0.03),
                        hintStyle: TextStyle(
                            color: Colors.black45, fontSize: _height * 0.03)),
                  ),
                ),
              ),
              SizedBox(
                height: _height * 0.02,
              ),
              Padding(
                padding: EdgeInsets.only(right: _height * 0.03),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'رمزعبورخودرافراموش کردم؟',
                    style: TextStyle(
                        color: Colors.black45, fontSize: _height * 0.03),
                  ),
                ),
              ),
              SizedBox(
                height: _height * 0.04,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(29),
                    ),
                    fixedSize: Size(_width * 0.9, _height * 0.06)),
                child: Text(
                  'ورود',
                  style:
                      TextStyle(fontSize: _height * 0.04, color: Colors.black),
                ),
                onPressed: () {
                  //Main Navigaton
                },
              ),
              SizedBox(
                height: _height * 0.03,
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child: Padding(
                  padding: EdgeInsets.only(top: _height * 0.04),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'کاربرجدید؟',
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontSize: _height * 0.03,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .pushReplacementNamed(RegisterScreen.routeName);
                        },
                        child: Text(
                          'ثبت نام',
                          textDirection: TextDirection.rtl,
                          style: TextStyle(
                              fontSize: _height * 0.03,
                              fontFamily: 'BNaznnBd',
                              color: Colors.blue[700],
                              fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ]),
          )
        ],
      ),
    ));
  }
}
