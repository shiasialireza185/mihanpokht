import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import '../widgets/category_item.dart';

class ProductsOverViewScreen extends StatelessWidget {
  static const routeName = '/food-overviewscreen';
  ProductsOverViewScreen({Key? key}) : super(key: key);
  var userName = 'علیرضا';
  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Directionality(
            textDirection: TextDirection.rtl,
            child: Padding(
              padding: EdgeInsets.only(
                top: _height * 0.05,
              ),
              child: Row(
                children: [
                  SizedBox(
                    width: _width * 0.02,
                  ),
                  Container(
                    padding: EdgeInsets.all(_height * 0.005),
                    // height: _height * 0.09,
                    // width: _width * 0.2,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(29),
                        color: Colors.blue[300]),
                    child: Icon(
                      Icons.person,
                      size: _height * 0.05,
                    ),
                    // child: CircleAvatar(
                    //   child: Icon(
                    //     Icons.person,
                    //     textDirection: TextDirection.rtl,
                    //     size: _height * 0.04,
                    //   ),
                    // ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: _width * 0.03),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Text('سلام',
                                style: TextStyle(
                                    fontSize: _height * 0.03,
                                    color: Colors.black)),
                            Text(
                              ',عیرضا',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: _height * 0.03,
                                  color: Colors.black),
                            ),
                          ],
                        ),
                        Text(
                          'چه غذایی امروز میل دارین',
                          style: TextStyle(
                              fontSize: _height * 0.03, color: Colors.black54),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Divider(
            height: _height * 0.03,
            color: Colors.black,
          ),
          Padding(
            padding:
                EdgeInsets.only(right: _height * 0.02, left: _height * 0.02),
            child: Container(
              width: double.infinity,
              height: _height * 0.05,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(width: _width * 0.003, color: Colors.grey),
                  color: Colors.grey[100]),
              child: Directionality(
                textDirection: TextDirection.rtl,
                child: TextField(
                  cursorHeight: _height * 0.04,
                  textAlignVertical: TextAlignVertical.center,
                  style: TextStyle(fontSize: _height * 0.03),
                  textDirection: TextDirection.rtl,
                  textAlign: TextAlign.right,
                  decoration: InputDecoration(
                      hintText: 'جست وجوی غذا',
                      hintStyle: TextStyle(
                        fontSize: _height * 0.03,
                        fontWeight: FontWeight.w300,
                        color: Colors.black38,
                      ),
                      border: InputBorder.none,
                      icon: Icon(
                        Icons.search,
                        size: _height * 0.04,
                        color: Colors.black26,
                      )),
                ),
              ),
            ),
          ),
          // SizedBox(height: _height * 0.02),

          Padding(
            padding: EdgeInsets.all(_width * 0.04),
            child: Container(
              width: double.infinity,
              height: _height * 0.05,
              child: ListView.builder(
                  // shrinkWrap: true,
                  // physics: NeverScrollableScrollPhysics(),
                  itemCount: 3,
                  itemBuilder: (ctx, index) => const CategoryItem(),
                  scrollDirection: Axis.horizontal),
            ),
          ),
        ],
      ),
    );
  }
}
