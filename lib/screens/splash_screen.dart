import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import '../screens/login_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    splashScreen(context);
    super.initState();
  }

  void splashScreen(BuildContext context) {
    Timer(const Duration(seconds: 3), () {
      Navigator.of(context)
          .pushNamedAndRemoveUntil(LoginScreen.routeName, (route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    // final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 226, 96, 10),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(top: _height * 0.25),
            child: Expanded(
              flex: 3,
              child: Align(
                alignment: Alignment.center,
                child: Image.asset(
                  'assets/images/splashPic.png',
                  fit: BoxFit.contain,
                  width: double.infinity,
                  height: _height * 0.50,
                ),
              ),
            ),
          ),
          const Expanded(
            flex: 3,
            child: Align(
              alignment: Alignment.center,
            ),
          ),
          const CircularProgressIndicator(
            color: Colors.blue,
            strokeWidth: 5,
          ),
          Expanded(
              flex: 5,
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: EdgeInsets.all(_height * 0.02),
                  child: Text(
                    'V 1.1.0',
                    style: TextStyle(
                        fontSize: _height * 0.03, color: Colors.white),
                  ),
                ),
              ))
        ],
      ),
    );
  }
}
