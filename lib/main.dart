import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'screens/splash_screen.dart';
import 'package:device_preview/device_preview.dart';
import './screens/login_screen.dart';
import './screens/register_screen.dart';
import 'screens/products_overview_screen.dart';

void main() {
  runApp(DevicePreview(
    builder: (ctx) => const MyApp(),
    enabled: !kReleaseMode,
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      locale: DevicePreview.locale(context),
      builder: DevicePreview.appBuilder,
      theme: ThemeData(
        fontFamily: 'BNazanin',
        primaryColor: const Color.fromARGB(255, 226, 96, 10),
        // colorScheme: const ColorScheme.light(primary: Colors.black)),
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (ctx) => const SplashScreen(),
        // '/': (ctx) => ProductsOverViewScreen(),
        LoginScreen.routeName: (ctx) => const LoginScreen(),
        RegisterScreen.routeName: (ctx) => const RegisterScreen(),
        ProductsOverViewScreen.routeName: (ctx) => ProductsOverViewScreen()
      },
    );
  }
}
