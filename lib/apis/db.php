<?php
$serverName="localhost";
$userName="root";
$password="";
$dbname="website";

$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully";
?>

try{

    $con=new PDO("mysql:host=$serverName;dbname=$dbname",$userName,$password);
    $con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
   // echo "Connection Success";
}catch(PDOException $e){
    echo "Error in Connection " . $e->getMessage();
}

?>