import 'package:flutter/cupertino.dart';
import '../providers/product.dart';

class Products with ChangeNotifier {
  final List<Product> _items = [
    Product(
        id: '1',
        name: 'چلوکباب',
        price: 25.000,
        imageUrl: '',
        description: ' 250 گرم گوشت گوسفند کبابی وبرنج ایرانی')
  ];
  List<Product> get items {
    //create getter for get products items
    return [..._items];
  }
}
