import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class Product with ChangeNotifier {
  final String id;
  final String name;
  final String imageUrl;
  final double price;
  final String description;
  bool isFavorite;

  Product(
      {required this.id,
      required this.name,
      required this.price,
      required this.imageUrl,
      required this.description,
      this.isFavorite = false});
}
